import pytest

pyadd = lambda a, b: a + b

@pytest.mark.parametrize(
    "ins, outs", 
    [
        ((0, 0), 0), 
        ((2, 5), 7), 
        ((-2, 5), "oops")
    ]
)
def test_parametrized(ins, outs):
    result = pyadd(*ins)
    assert result == outs, f"{result=}"
