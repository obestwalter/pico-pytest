import pytest

@pytest.mark.charlie  # any valid identifier allowed
def test_fails_marked_charlie(): charlie_alone_fails
    
@pytest.mark.lucy
def test_passes_marked_lucy(): ...

@pytest.mark.lucy
@pytest.mark.charlie
def test_passes_marked_lucy_and_charlie(): ...
