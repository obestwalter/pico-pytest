
import pytest

@pytest.fixture # register this function as fixture
def the_answer():
    return 42

# request the fixture result via its name
def test_passes_fixture(the_answer):
    assert the_answer == 42

def test_fails_fixture(the_answer):
    assert the_answer == 23, f"NO! {the_answer=} :("
