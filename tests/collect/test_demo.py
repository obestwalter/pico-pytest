
import sys

test_global_atttribute = "I am not a test!"

def test_passes():
    print("I am a passing test", file=sys.stderr)

def test_fails_assert():
    assert 0, "assertion fails => test fails"

def test_fails_exception():
    int("x")  # error anywhere => test fails

def non_test_function():
    raise RuntimeError("I am not a test!")
