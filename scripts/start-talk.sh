#!/usr/bin/env bash

clear

SCRIPTS="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
PROJECT_ROOT="$(dirname $SCRIPTS)"
cd "$PROJECT_ROOT"

find . -type d -name "__pycache__" -exec rm -rf "{}" \; 2> /dev/null || true

deactivate 2>/dev/null || true  # in case we are in a normal venv
mamba activate jn6 2>/dev/null

export PIP_INDEX_URL="https://pypi.org/simple"
export PIP_EXTRA_INDEX_URL="https://pypi.org/simple"

export JUPYTER_PORT=8889
TALK=$1


$SCRIPTS/configure-rise.py
$SCRIPTS/count-slides.sh $TALK

#export COLUMNS=54  # ensure that pytest separators are never too wide

JUPYTER_TOKEN=ollitoken jupyter trust $TALK
JUPYTER_TOKEN=ollitoken jupyter notebook -y --no-mathjax $TALK
