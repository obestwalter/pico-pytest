#!/usr/bin/env bash

TALK=$1

echo "#####################################"
echo "$TALK slides: $(cat $TALK | grep -e '"slide_type": ".*slide"' | wc -l)"
echo "#####################################"
echo ""
