#!/usr/bin/env python3

from traitlets.config.manager import BaseJSONConfigManager
from pathlib import Path
path = Path.home() / ".jupyter" / "nbconfig"
cm = BaseJSONConfigManager(config_dir=str(path))
cm.update(
    "rise",
    {
        "start_slideshow_at": "selected",
        "autolaunch": False,
        "controls": False,
        "progress": True,
        "slideNumber": "h.v",  # c would be flat
        "scroll": True,  # make whole slide scrollable if too long
        "center": True,
        "width": "100%",
        "height": "100%",
        # https://revealjs.com/themes/
        # "theme": "simple",
        "theme": "solarized",
        # "theme": "beige",
        # "theme": "sky",
        "auto_select": "code",
        "auto_select_fragment": True,
        "transition": "fade",  # none/fade/slide/convex/concave/zoom
        "transitionSpeed": "fast",  # // default/fast/slow
        "enable_chalkboard": False,
        # "shortcuts": {}
    }
)

# https://rise.readthedocs.io/en/latest/customize.html#reveal-js-configuration-options
# https://revealjs.com/config/

r"""
action name                key      behaviour
------------------------------------------------------
RISE:slideshow            alt-r  enter/exit RISE Slideshow
RISE:smart-exec                  execute cell, move to the next if on same slide
RISE:toggle-slide        shift-i (un)set current cell as a Slide cell
RISE:toggle-subslide     shift-b (un)set current cell as a Sub-slide cell
RISE:toggle-fragment     shift-g (un)set current cell as a Fragment cell
RISE:toggle-notes                (un)set current cell as a Notes cell
RISE:toggle-skip                 (un)set current cell as a Skip cell
RISE:render-all-cells            render all cells (all cells go to command mode)
RISE:edit-all-cells              edit all cells (all cells go to edit mode)
RISE:rise-nbconfigurator shift-c open the nbconfigurator pane in another tab

native reveal.js shortcuts

module      action               default key  behaviour
---------------------------------------------------------
main        firstSlide           home         jump to first slide
main        lastSlide            end          jump to last slide
main        toggleOverview       w            toggles slide overview
main        fullscreenHelp       f            show fullscreen help
main        riseHelp             ?            show the RISE help
chalkboard  clear                -            clear full size chalkboard
chalkboard  reset                =            reset chalkboard data on current slide
chalkboard  toggleChalkboard     [            toggle full size chalkboard
chalkboard  toggleNotesCanvas    ]            toggle notes (slide-local)
chalkboard  download             \            download recorded chalkboard drawing
"""
