#!/usr/bin/env bash

time jupyter execute pipy-full.ipynb
cd /tmp/pp/
ls -la .

export PIP_INDEX_URL="https://pypi.org/simple"
export PIP_EXTRA_INDEX_URL="https://pypi.org/simple"

pip install --editable .
pipy || true  # TODO check output against known good with e.g. pytest-golden
