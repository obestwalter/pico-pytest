#!/usr/bin/env bash

# Installing rise with pip results in jupyterlab being installed!?!?!
mamba create -y -n jn6 python=3.12
mamba activate jn6
mamba install -y pytest pygments

# (I want alt+q for re-render) pre-release version of rise not available via mamba
# Still installing it that way as that does not draw in jupyterlab
pip install --upgrade --pre "rise==5.7.2.dev2"

# to use the split cell extension feature (put two cells next to each other)
mamba install -y jupyter_contrib_nbextensions
pip install "jupyter-server<2" || true
echo "pip is unhappy - things work anyway - yes, it's a hack."

python $SCRIPTS/configure-rise.py
